begin transaction;
drop table if exists lifetime;
create table lifetime (
    id integer primary key autoincrement,
    title text not null,
    organization text not null,
    location text,
    type text,
    from_date text,
    to_date text
);
drop table if exists skill;
create table skill (
    id integer primary key autoincrement,
    technology text not null,
    category text,
    level integer,
    love integer
);
drop table if exists portfolio;
create table portfolio (
    id integer primary key autoincrement,
    technology text not null,
    name text not null,
    url text not null
);
commit;