from flask import Flask, abort, jsonify
from cvapp import app, views
        
@app.route('/')
def usage():
    # Available endpoints match available view methods
    endpoints = [name for name in dir(views) if not name.startswith('__')]
    endpoints.remove('db') # imported module, not an endpoint!
    return jsonify(
        {
            'implementation': "flask",
            'code': "https://bitbucket.org/marbartolome/cvapp",
            'endpoints': endpoints
        })

@app.route('/<view>')
def view(view):
    #Call requested view from views module and jsonify it. 404 if invalid view.
    try:
        return jsonify({view: getattr(views, view)()})
    except AttributeError:
        abort(404) 
