from flask import Flask
import os

app = Flask(__name__)
app.config.from_object(__name__)

import cvapp.endpoints

# Dev settings
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, '../../db.db'),
    DEBUG=True,
    SECRET_KEY='development'
))

# Prod settings in config file pointed by envvar
app.config.from_envvar('CVAPP_SETTINGS', silent=True)

