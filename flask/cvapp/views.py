from cvapp import db

def cv():
    return {
        "academic": academic(),
        "jobs": jobs(),
        "skills": skills()
        }

def skills():
    skills = db.query("select * from skill")
    for skill in skills:
        projects = db.query (
            "select * from portfolio where technology = ?", 
            [skill['technology']])
        if projects: skill['portfolio'] = projects
    return skills

def academic():
    return db.query("select * from lifetime where type = 'academic'")

def jobs():
    return db.query("select * from lifetime where type = 'job'")  

