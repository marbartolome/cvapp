import sqlite3
from flask import Flask, g
from cvapp import app

def connect_db():
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db 

@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()
        
def query(query, params=[]):
    """
    Poorman's ORM.
    Makes a database query, and transforms the result into a list
    of dicts. Each row is turned into a dict whose keys are 
    the database column names, and the values the actual values.
    """
    cursor = get_db().execute(query, params)
    rows = []
    for entry in cursor.fetchall():
        elem = {}
        keys = entry.keys()
        for key in keys:
            if key != "id": # skip primary key id
                elem[key]=entry[key]
        rows.append(elem)
    return rows