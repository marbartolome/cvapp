import cvapp
import unittest
import json
from mock import MagicMock

class FlaskrTestCase(unittest.TestCase):
    
    def setUp(self):
        cvapp.app.config['TESTING'] = True
        self.app = cvapp.app.test_client()
        
    def test_usage_endpoint(self):
        """Usage endpoint should list the 
        available endpoints"""
        rv = self.app.get('/')
        expected = {
            'implementation': "flask",
            'code': "https://bitbucket.org/marbartolome/cvapp",
            'endpoints':
                ['academic', 'cv', 'jobs','skills']
            }
        self.assertEqual(json.loads(rv.data), expected)
        
    def test_valid_view_endpoints(self):
        """When a valid view endpoint is requested,
        what comes out of the view should be turned to json"""
        mock_response = [
            {'key': 'value'},
            {'other_key': 'other_value'}]
        cvapp.views.academic = MagicMock(return_value=mock_response)
        cvapp.views.cv = MagicMock(return_value=mock_response)
        cvapp.views.jobs = MagicMock(return_value=mock_response)
        cvapp.views.skills = MagicMock(return_value=mock_response)
        
        for endpoint in ['academic', 'cv', 'jobs','skills']:
            rv = self.app.get('/{0}'.format(endpoint))
            self.assertEqual(json.loads(rv.data), {endpoint: mock_response})

    def test_empty_view_endpoint(self):
        """When asking for a valid view, a json
        should be returned, even if the view
        result is empty"""
        mock_response = []
        cvapp.views.academic = MagicMock(return_value=mock_response)
        cvapp.views.cv = MagicMock(return_value=mock_response)
        cvapp.views.jobs = MagicMock(return_value=mock_response)
        cvapp.views.skills = MagicMock(return_value=mock_response)
        
        for endpoint in ['academic', 'cv', 'jobs','skills']:
            rv = self.app.get('/{0}'.format(endpoint))
            self.assertEqual(json.loads(rv.data), {endpoint: mock_response})
            
    def test_invalid_view_endpoint(self):
        """When an invalid view is requested, 
        404 is expected"""
        rv = self.app.get('/foo')
        self.assertEqual(rv.status_code, 404)

if __name__ == '__main__':
    unittest.main()