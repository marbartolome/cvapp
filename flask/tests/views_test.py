import cvapp
import unittest
import json
from mock import MagicMock

class FlaskrTestCase(unittest.TestCase):
    
    def setUp(self):
        cvapp.app.config['TESTING'] = True
        self.app = cvapp.app.test_client()

    def tearDown(self):
        pass
    
    def test_jobs(self):
        """When we request the jobs view,
        we should return what comes out of the database.
        A field 'type' with value 'job' should be present.
        """
        mock_response = [
            {
                'organization': "stark industries",
                'location': "NYC",
                'title': "super lead research scientist",
                'from_date': "January 2002",
                'to_date': "December 2222",
                'type': "job"
            }
        ]
        cvapp.db.query = MagicMock(return_value=mock_response)
        
        view_res = cvapp.views.jobs()
        
        self.assertEqual(view_res[0]['type'], "job")
        self.assertEqual(view_res, mock_response)
        
    def test_jobs_empty(self):
        """When the database returns no jobs
        we should still return an empty list"""
        cvapp.db.query = MagicMock(return_value=[])
        
        view_res = cvapp.views.jobs()
        
        self.assertEqual(view_res, [])
        
    def test_academic(self):
        """When we request the academic view,
        we should return what comes out of the database.
        A field 'type' with value 'academic' should be present.
        """
        mock_response = [
            {
                'organization': "harvard",
                'location': "Cambridge, MA",
                'title': "PhD in evil genius",
                'from_date': "September 2010",
                'to_date': "June 2011",
                'type': "academic"
            }
        ]
        cvapp.db.query = MagicMock(return_value=mock_response)
        
        view_res = cvapp.views.academic()
        
        self.assertEqual(view_res[0]['type'], "academic")
        self.assertEqual(view_res, mock_response)
        
    def test_academic_empty(self):
        """When the database returns nothing
        we should still return an empty list"""
        cvapp.db.query = MagicMock(return_value=[])
        
        view_res = cvapp.views.academic()
        
        self.assertEqual(view_res, [])
        
    def test_skills(self):
        """When we request skills we should
        get the list of skills augmented with
        portfolio info, if this exists.
        """
        def mock_db_query(query, args = []):
            if query == "select * from skill":
                return [
                    {
                        'category': "languages",
                        'level': 3,
                        'love': 4,
                        'technology': "python"
                    },
                    {
                        'category': "languages",
                        'level': 0,
                        'love': 4,
                        'technology': "haskell"
                    },
                ]
            elif "python" in args:
                return []
            elif "haskell" in args:
                return [
                    {
                        'name': "Haskell Tubes",
                        'technology': "haskell",
                        'url': "https://bitbucket.org/marbartolome/haskell_tubes"
                    }
                ]
        cvapp.db.query = MagicMock(side_effect=mock_db_query)

        view_res = cvapp.views.skills()
                
        python_item = view_res[0]
        haskell_item = view_res[1]
        
        self.assertEqual(python_item['technology'], "python")
        self.assertEqual(haskell_item['technology'], "haskell")
        
        self.assertIsNone(python_item.get('portfolio', None))
        self.assertEqual(len(haskell_item['portfolio']), 1)
        
if __name__ == '__main__':
    unittest.main() 