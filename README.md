# CV as a service

A RESTful service exposing CV data. For now, a two tiered architecture:

- REST API (implementations: flask)
- datastore (implementations: SQLite)

Plans are to include alternative implementations of each of the tiers, to showcase different technologies.

## Usage

The `/` endpoint should list the available endpoints you can request.

## Live demo

http://mar-cvapp.herokuapp.com/
