begin transaction;

delete from lifetime;
insert into lifetime (title, organization, location, type, from_date, to_date) values 
    ("BSc in Software Engineering", "Málaga University", "Málaga", "academic", "September 2006", "June 2011"),
    ("Server Developer", "Sony Computer Entertainment Europe London Studio", "London", "job", "January 2014", "Now"),
    ("Software Engineer", "Musicmetric", "London", "job", "September 2012", "December 2013"),
    ("Junior Developer", "Yerbabuena Software", "Málaga", "job", "October 2011", "August 2012"),
    ("Intern Software Developer", "Irish Center for High-­End Computing", "Málaga", "job", "June 2011", "August 2011"),
    ("Teacher-aiding Intern", "Málaga University", "Málaga", "job", "January 2010", "June 2010"),
    ("Intern Programmer", "Proneotec", "Málaga", "job", "October 2009", "January 2010")
;
delete from skill;
insert into skill (technology, category, level, love) values 
    ("ruby", "languages", 1, null),
    ("php", "languages", 2, 1),
    ("erlang", "languages", 1, 4),
    ("python", "languages", 3, 4),
    ("haskell", "languages", 0, 4),
    ("java", "languages", 3, 3),
    ("hadoop", "big data", 1, null),
    ("sqlite", "databases", null, 5),
    ("bash", "devops", null, 4),
    ("javascript", "languages", 0, 3),
    ("git", "dvcs", 3, 5),
    ("c++", "languages", 1, 2),
    ("c", "languages", 1, 2),
    ("chef", "devops", 1, null),
    ("postgresql", "databases", 3, 4),
    ("voldemort", "databases", 1, null),
    ("libgdx", "gamedev framework", 1, null),
    ("aws", "infrastructure", 1, null)
;
delete from portfolio;
insert into portfolio (technology, name, url) values 
    ("java", "Dynamus", "http://gitorious.org/dynamus"),
    ("java", "The Master of All Magic", "https://bitbucket.org/marbartolome/ld27"),
    ("java", "Naughts and Crosses Game Server", "https://bitbucket.org/marbartolome/sonynaughts_spring"),
    ("java", "Light's Out Solver", "https://gitorious.org/light-s-out-solver"),
    ("haskell", "London Tube Route Finder","http://bitbucket.org/marbartolome/haskell_tubes"),
    ("c", "Toy Shell","https://gitorious.org/toy-unix-shell"),
    ("python", "CV App","https://bitbucket.org/marbartolome/cvapp"),
    ("python", "Twitter Hostiabot","https://gitorious.org/twitter-hostiabot"),
    ("javascript", "Greasemonkey Sentiment Analyzer","http://userscripts.org/scripts/show/154496"),
    ("javascript", "Hotel Router","https://github.com/marbru/hotelrouter"),
    ("javascript", "Audioship and Echodoge","https://bitbucket.org/coconauts/musichackday2013"),
    ("c++", "Watchduino","https://bitbucket.org/rephus/watchduino")
;

commit; 
